/*Instructions that can be provided to the students for reference:
Activity:
1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
14. Create a git repository named S23.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

function trainer(name, age, pokemon, friends, talk){
  this.name = name;
  this.age = age;
  this.pokemon = pokemon;
  this.friends = friends;
  this.talk = function() {
          console.log(Player.pokemon[0] + '!' + "I choose you");
          
          };

}


let Player = new trainer('Ash Ketchum', 10, ["Pikachu", "Charizard", "Squitle", "Bulbasaur"], {hoenn: ["May","Max"], kanto: ["Brock", "Misty"]});
console.log(Player);

console.log(Player.name);
console.log(Player[0]);
console.log("Result of Talk method");
Player.talk();

// let Player = new trainer('Ash Ketchum', 10, 'Pikachu')



// Creating an object constructor instead will help with this process
  function Pokemon(name, level, health, attack, tackle) {

      // Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = attack;

      


      //Methods
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          };

      this.faint = function(){
             if(this.health == 0 || this.health < -1){
        console.log(this.name + " is dead")
      }else{
        console.log(this.name + "still fighting")
      }

      }

        

  }

  // Creates new instances of the "Pokemon" object each with their unique properties
  let pikachu = new Pokemon("Pikachu", 12, 24, 12);
  let geodude = new Pokemon('Geodude', 8, 16, 8);
  let mewtwo = new Pokemon('Mewtwo', 100, 200, 100);




  console.log(pikachu)
  console.log(geodude)
  console.log(mewtwo)

  // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
  geodude.tackle(pikachu);
   mewtwo.tackle(geodude);
   geodude.faint();


